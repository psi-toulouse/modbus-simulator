/* lightweight logging library */
#ifndef LOG_H_
#define LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

	typedef enum 
	{
		LOG_CRITICAL,		//	0
		LOG_ERROR,			//	1
		LOG_WARN,			//	2
		LOG_INFO,			//	3
		LOG_DEBUG,			//	4
		LOG_NOTHING			//	5
	} mylog_level;

	/* configuration */
	void mylog_init(const char* path, mylog_level loglevel);

	/* logging functions */
	void mylog_debug(const char* format, ...); /* debug log */
	void mylog_info(const char* format, ...); /* debug log */
	void mylog_warn(const char* format, ...); /* debug log */
	void mylog_error(const char* format, ...); /* debug log */
	void mylog_critical(const char* format, ...); /* debug log */

#ifdef __cplusplus
}
#endif

#endif /* LOG_H_ */

