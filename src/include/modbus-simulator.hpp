/* -----------------------------------------------------------------------------
 * modbus-simulator (C) 2020 - Philippe SICARDI 
 * ----------------------------------------------------------------------------- */

#include "mylog.hpp"

// Version du logiciel
#define VERSION_MAJ	1
#define VERSION_MIN	0

// Nombre maximum de registres par serveur dans le fichier fichier de configuration
#define MAX_PARAMETERS	1024


// Nombre maximal de connexion acceptées en mode SERVER
#define	MAX_CONNEXION	16

#define UPDATE_NULL				0
#define	UPDATE_INCREMENT		1
#define	UPDATE_DECREMENT		2
#define	UPDATE_RANDOM			3
#define	UPDATE_ALTERNATE		4

#define TYPE_NULL				0
#define TYPE_BITS				1
#define TYPE_INPUT_BITS			2
#define TYPE_REGISTERS			3
#define TYPE_INPUT_REGISTERS	4

#define	NO_ERROR				0
#define	ERROR_FILE				-1
#define	ERROR_PARSING			-2
#define	ERROR_PARAMETER			-3
#define	ERROR_MISSING_VALUE		-4
#define	ERROR_BAD_VALUE			-5

const char*szMappingType[] = {
	"Undefined",
	"Bits",
	"Input Bits",
	"Registers",
	"Input Registers"
};


struct myParameter {
	int		type = 0;
	int 	address = 0;
	int 	set = 0;
	int 	update = 0;
	int		min = 0;
	int		max = 0;
	int		reset = 0;
};



struct myConfig {

	int		update_freq = 60;
	int		nb_params = 0;
	int		port = 502;
	
	int		bits_address = 0;
	int		bits_nb = 0;
	
	int		input_bits_address = 0;
	int		input_bits_nb = 0;

	int		registers_address = 0;
	int		registers_nb = 0;
	
	int		input_registers_address = 0;
	int		input_registers_nb = 0;

	mylog_level	log_level = LOG_NOTHING;
	char		log_path[256] = { 0 };
	
	struct	myParameter parameters[MAX_PARAMETERS];
	
};

