#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/timeb.h>
#include <sys/time.h>
#include <time.h>

#include "mylog.hpp"

#define MAX_PATH_LENGTH 512

static mylog_level loglevel = LOG_INFO;
static char logpath[MAX_PATH_LENGTH] = { 0 };

void mylog_init(const char* path, mylog_level level) 
{
	FILE* fp = NULL;

	loglevel = level;
	strncpy(logpath,path,MAX_PATH_LENGTH);
	fp = fopen(logpath, "a");
	if (fp == NULL) 
	{
		strcpy(logpath,"/dev/stdout");
	}
	fclose(fp);
}


static char* get_timestamp(char* buf) 
{
	struct timeval	tv;
    struct tm *t;
	
	gettimeofday(&tv,(struct timezone *) 0);
    t = localtime(&tv.tv_sec);

	sprintf(buf,"[%02d/%02d/%04d %02d:%02d:%02d,%03d]",
		t->tm_mday,t->tm_mon+1,t->tm_year+1900,t->tm_hour,t->tm_min,t->tm_sec,(int)(tv.tv_usec/1000));

	return buf;
}

void mylog_critical(const char* format, ...) 
{
	char tmp[50] = { 0 };
	FILE* fp = NULL;

	if (loglevel < LOG_CRITICAL) return;

	fp = fopen(logpath, "a");
	if (fp == NULL) return;

	va_list args;
	va_start (args, format);
	fprintf(fp, "%s - CRITICAL - ", get_timestamp(tmp));
	vfprintf (fp, format, args);
	va_end (args);
	fprintf(fp,"\n");
	fflush(fp);
	fclose(fp);
}

void mylog_error(const char* format, ...) 
{
	char tmp[50] = { 0 };
	FILE* fp = NULL;

	if (loglevel < LOG_ERROR) return;

	fp = fopen(logpath, "a");
	if (fp == NULL) return;

	va_list args;
	va_start (args, format);
	fprintf(fp, "%s - ERROR - ", get_timestamp(tmp));
	vfprintf (fp, format, args);
	va_end (args);
	fprintf(fp,"\n");
	fflush(fp);
	fclose(fp);
}

void mylog_warn(const char* format, ...) 
{
	char tmp[50] = { 0 };
	FILE* fp = NULL;

	if (loglevel < LOG_WARN) return;

	fp = fopen(logpath, "a");
	if (fp == NULL) return;

	va_list args;
	va_start (args, format);
	fprintf(fp, "%s - WARN - ", get_timestamp(tmp));
	vfprintf (fp, format, args);
	va_end (args);
	fprintf(fp,"\n");
	fflush(fp);
	fclose(fp);
}

void mylog_info(const char* format, ...) 
{
	char tmp[50] = { 0 };
	FILE* fp = NULL;

	if (loglevel < LOG_INFO) return;

	fp = fopen(logpath, "a");
	if (fp == NULL) return;

	va_list args;
	va_start (args, format);
	fprintf(fp, "%s - INFO - ", get_timestamp(tmp));
	vfprintf (fp, format, args);
	va_end (args);
	fprintf(fp,"\n");
	fflush(fp);
	fclose(fp);
}

void mylog_debug(const char* format, ...) 
{
	char tmp[50] = { 0 };
	FILE* fp = NULL;

	if (loglevel < LOG_DEBUG) return;

	fp = fopen(logpath, "a");
	if (fp == NULL) return;

	va_list args;
	va_start (args, format);
	fprintf(fp, "%s - DEBUG - ", get_timestamp(tmp));
	vfprintf (fp, format, args);
	va_end (args);
	fprintf(fp,"\n");
	fflush(fp);
	fclose(fp);
}
