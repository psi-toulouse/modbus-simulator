#include <iostream>
#include <json.h>
#include <uuid/uuid.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#include <sys/timeb.h>
#include <sys/select.h>

#include <sys/socket.h>
#include <netinet/in.h>

#include <modbus.h>

#include "modbus-simulator.hpp"

#include "mylog.hpp"

using namespace std;

struct myConfig config;

modbus_mapping_t *mb_mapping;

char szConfigFile[127+1];
// static int counter = 0;

long long time_ref = 0LL;

// ------------------------------------------------------------------------
// ctx.s = socket
// https://github.com/stephane/libmodbus/blob/master/src/modbus-private.h
// ------------------------------------------------------------------------

int iFin = 0;

long long current_timestamp() 
{
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    return milliseconds;
}

int parseConfigFile(const char *file)
{
	char *json;
	int fd;
	struct json_object *obj, *params_array, *params_obj, *tmp, *update, *options;
	struct stat st;

	fd = open(file, O_RDONLY); 
	if (fd == NULL) return(ERROR_FILE);
	if(fstat(fd,&st) < 0) return(ERROR_FILE);
	json = (char *) mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);

	obj = json_tokener_parse(json);
	if (obj == NULL) {
		mylog_error("Parsing error");
		return(ERROR_PARSING);
	}

	json_object_object_foreach(obj, key, val) 
	{
		if (config.log_level != LOG_NOTHING && config.log_path[0] != '\0')
		{
			mylog_init(config.log_path,config.log_level);
		}

//		printf("Hey! key = %s value = %s\n",key, json_object_get_string(val));
		
		if (config.nb_params > MAX_PARAMETERS)
		{
			mylog_error("Parameters configured (%d) exceed the limit (%d)",config.nb_params,MAX_PARAMETERS);
			return(ERROR_PARAMETER);
		}			
		
		int val_type = json_object_get_type(val);
		switch (val_type)
		{
			case json_type_double:
			case json_type_int:
			case json_type_boolean:
				if (strcmp(key,"update_freq") == 0) config.update_freq = json_object_get_int(val);
				if (strcmp(key,"port") == 0) config.port = json_object_get_int(val);
				if (strcmp(key,"log_level") == 0) config.log_level = (mylog_level)  json_object_get_int(val);
				
				if (strcmp(key,"bits_address") == 0) config.bits_address = json_object_get_int(val);
				if (strcmp(key,"bits_nb") == 0) config.bits_nb = json_object_get_int(val);
				if (strcmp(key,"input_bits_address") == 0) config.input_bits_address = json_object_get_int(val);
				if (strcmp(key,"input_bits_nb") == 0) config.input_bits_nb = json_object_get_int(val);

				if (strcmp(key,"registers_address") == 0) config.registers_address = json_object_get_int(val);
				if (strcmp(key,"registers_nb") == 0) config.registers_nb = json_object_get_int(val);
				if (strcmp(key,"input_registers_address") == 0) config.input_registers_address = json_object_get_int(val);
				if (strcmp(key,"input_registers_nb") == 0) config.input_registers_nb = json_object_get_int(val);
				
				break;

			case json_type_string:
				if (strcmp(key,"log_path") == 0) strcpy(config.log_path,json_object_get_string(val));
				break;

			case json_type_array:
				if (strcmp(key,"registers") == 0 || 
					strcmp(key,"input_registers") == 0 ||
					strcmp(key,"bits") == 0 ||
					strcmp(key,"input_bits") == 0 )
				{
//printf("HELLO : %s\n",json_object_get_string(val));
					for (int k=0;k<json_object_array_length(val);k++)
					{
						params_obj = json_object_array_get_idx(val,k);
						
						if (strcmp(key,"registers") == 0) config.parameters[config.nb_params].type = TYPE_REGISTERS;
						if (strcmp(key,"input_registers") == 0) config.parameters[config.nb_params].type = TYPE_INPUT_REGISTERS;
						if (strcmp(key,"bits") == 0) config.parameters[config.nb_params].type = TYPE_BITS;
						if (strcmp(key,"input_bits") == 0) config.parameters[config.nb_params].type = TYPE_INPUT_BITS;
						
						json_object_object_get_ex(params_obj,"address",&tmp);
						if (tmp == NULL) {
							mylog_error("%s address not declared in configuration file",
								szMappingType[config.parameters[config.nb_params].type]);
							return(ERROR_MISSING_VALUE);
						} else
						{
							config.parameters[config.nb_params].address = json_object_get_int(tmp);
							switch (config.parameters[config.nb_params].type)
							{
								case TYPE_REGISTERS:
									if (config.parameters[config.nb_params].address < config.registers_address ||
										config.parameters[config.nb_params].address >= (config.registers_address + config.registers_nb))
										{
											mylog_error("Address (%d) out of range [%d,%d] in : %s",
												config.parameters[config.nb_params].address,
												config.registers_address,
												(config.registers_address + config.registers_nb),json_object_get_string(params_obj));
											return(ERROR_BAD_VALUE);
										}
									break;
									
								case TYPE_INPUT_REGISTERS:
									if (config.parameters[config.nb_params].address < config.input_registers_address ||
										config.parameters[config.nb_params].address >= (config.input_registers_address + config.input_registers_nb))
										{
											mylog_error("Address (%d) out of range [%d,%d] in : %s",
												config.parameters[config.nb_params].address,
												config.input_registers_address,
												(config.input_registers_address + config.input_registers_nb),json_object_get_string(params_obj));
											return(ERROR_BAD_VALUE);
										}
									break;
									
								case TYPE_BITS:
									if (config.parameters[config.nb_params].address < config.bits_address ||
										config.parameters[config.nb_params].address >= (config.bits_address + config.bits_nb))
										{
											mylog_error("Address (%d) out of range [%d,%d] in : %s",
												config.parameters[config.nb_params].address,
												config.bits_address,
												(config.bits_address + config.bits_nb),json_object_get_string(params_obj));
											return(ERROR_BAD_VALUE);
										}
									break;
									
								case TYPE_INPUT_BITS:
									if (config.parameters[config.nb_params].address < config.input_bits_address ||
										config.parameters[config.nb_params].address >= (config.input_bits_address + config.input_bits_nb))
										{
											mylog_error("Address (%d) out of range [%d,%d] in : %s",
												config.parameters[config.nb_params].address,
												config.input_bits_address,
												(config.input_bits_address + config.input_bits_nb),json_object_get_string(params_obj));
											return(ERROR_BAD_VALUE);
										}
									break;
							}
						}

						json_object_object_get_ex(params_obj,"set",&tmp);
						if (tmp == NULL) {
							mylog_error("%s initial value not set in configuration file",
								szMappingType[config.parameters[config.nb_params].type]);
							return(ERROR_MISSING_VALUE);
						} else
							config.parameters[config.nb_params].set = json_object_get_int(tmp);

						json_object_object_get_ex(params_obj,"update",&update);
						if (update == NULL)
						{
							config.parameters[config.nb_params].update = UPDATE_NULL;
							mylog_info("%s at address (%d), initial value (%d)",
								szMappingType[config.parameters[config.nb_params].type],
								config.parameters[config.nb_params].address,
								config.parameters[config.nb_params].set);
						} else
						{
							switch (config.parameters[config.nb_params].type)
							{
								case TYPE_REGISTERS :
								case TYPE_INPUT_REGISTERS :
									json_object_object_get_ex(update,"increment",&options);
									if (options != NULL) {
										config.parameters[config.nb_params].update = UPDATE_INCREMENT;
										json_object_object_get_ex(options,"max",&tmp);
										if (tmp == NULL) {
											mylog_error("Max value for increment not defined in configuration file");
											return(ERROR_MISSING_VALUE);
										}
										config.parameters[config.nb_params].max = json_object_get_int(tmp);

										json_object_object_get_ex(options,"reset",&tmp);
										if (tmp == NULL) {
											mylog_error("Reset value for increment not defined in configuration file");
											return(ERROR_MISSING_VALUE);
										}
										config.parameters[config.nb_params].reset = json_object_get_int(tmp);
										
										mylog_info("%s at address (%d), initial value (%d), increment, reset to (%d) if greater than (%d)",
											szMappingType[config.parameters[config.nb_params].type],
											config.parameters[config.nb_params].address,
											config.parameters[config.nb_params].set,
											config.parameters[config.nb_params].reset,
											config.parameters[config.nb_params].max);
											
									}

									json_object_object_get_ex(update,"decrement",&options);
									if (options != NULL) {
										config.parameters[config.nb_params].update = UPDATE_DECREMENT;

										json_object_object_get_ex(options,"min",&tmp);
										if (tmp == NULL) {
											mylog_error("Min value for decrement not defined in configuration file");
											return(ERROR_MISSING_VALUE);
										}
										config.parameters[config.nb_params].min = json_object_get_int(tmp);

										json_object_object_get_ex(options,"reset",&tmp);
										if (tmp == NULL) {
											mylog_error("Reset value for decrement not defined in configuration file");
											return(ERROR_MISSING_VALUE);
										}
										config.parameters[config.nb_params].reset = json_object_get_int(tmp);
										
										mylog_info("%s at address (%d), initial value (%d), decrement, reset to (%d) if lower than (%d)",
											szMappingType[config.parameters[config.nb_params].type],
											config.parameters[config.nb_params].address,
											config.parameters[config.nb_params].set,
											config.parameters[config.nb_params].reset,
											config.parameters[config.nb_params].min);

									}

									json_object_object_get_ex(update,"random",&options);
									if (options != NULL) {
										config.parameters[config.nb_params].update = UPDATE_RANDOM;

										json_object_object_get_ex(options,"max",&tmp);
										if (tmp == NULL) {
											mylog_error("Max value for random not defined in configuration file");
											return(ERROR_MISSING_VALUE);
										}
										config.parameters[config.nb_params].max = json_object_get_int(tmp);

										json_object_object_get_ex(options,"min",&tmp);
										if (tmp == NULL) {
											mylog_error("Min value for random not defined in configuration file");
											return(ERROR_MISSING_VALUE);
										}
										config.parameters[config.nb_params].min = json_object_get_int(tmp);
										
										mylog_info("%s at address (%d), initial value (%d), randomized between (%d) to (%d)",
											szMappingType[config.parameters[config.nb_params].type],
											config.parameters[config.nb_params].address,
											config.parameters[config.nb_params].set,
											config.parameters[config.nb_params].min,
											config.parameters[config.nb_params].max);

									}
								break;
								
								case TYPE_BITS :
								case TYPE_INPUT_BITS :
//								printf("HELLO : %s\n",json_object_get_string(update));
									if (strcmp(json_object_get_string(update),"alternate") == 0) {
										config.parameters[config.nb_params].update = UPDATE_ALTERNATE;
										
										mylog_info("%s at address (%d), initial value (%d), alternate",
											szMappingType[config.parameters[config.nb_params].type],
											config.parameters[config.nb_params].address,
											config.parameters[config.nb_params].set);
									}
									
									if (strcmp(json_object_get_string(update),"random") == 0) {
										config.parameters[config.nb_params].update = UPDATE_RANDOM;
										
										mylog_info("%s at address (%d), initial value (%d), randomized",
											szMappingType[config.parameters[config.nb_params].type],
											config.parameters[config.nb_params].address,
											config.parameters[config.nb_params].set);
									}
								break;
							}
						}
						config.nb_params ++;
					}
				}
				break;
		}
	}

	return(NO_ERROR);
}

void initialize_register()
{
	uint8_t *b;
	uint16_t *r;

	mylog_debug("FCT initialize_register()");
	for (int i=0;i<config.nb_params;i++)
	{
		
		switch (config.parameters[i].type)
		{
			case TYPE_BITS:
				b = mb_mapping->tab_bits + (config.parameters[i].address - config.bits_address) ;
				break;

			case TYPE_INPUT_BITS:
				b = mb_mapping->tab_input_bits + (config.parameters[i].address - config.input_bits_address) ;
				break;

			case TYPE_REGISTERS:
				r = mb_mapping->tab_registers + (config.parameters[i].address - config.registers_address) ;
				break;

			case TYPE_INPUT_REGISTERS:
				r = mb_mapping->tab_input_registers + (config.parameters[i].address - config.input_registers_address) ;
				break;
		}

		switch (config.parameters[i].type)
		{
			case TYPE_BITS:
			case TYPE_INPUT_BITS:
				*b = config.parameters[i].set;
				break;
				
				
			case TYPE_REGISTERS:
			case TYPE_INPUT_REGISTERS:
				*r = config.parameters[i].set;
				break;		
		}
		
		switch (config.parameters[i].type)
		{
			case TYPE_BITS:
				mylog_debug("Initialize BIT at %d with %d",config.parameters[i].address,*b);
				break;
				
			case TYPE_INPUT_BITS:
				mylog_debug("Initialize INPUT BIT at %d with %d",config.parameters[i].address,*b);
				break;

			case TYPE_REGISTERS:
				mylog_debug("Initialize REGISTER at %d with %d",config.parameters[i].address,*r);
				break;
				
			case TYPE_INPUT_REGISTERS:
				mylog_debug("Initialize INPUT REGISTER at %d with %d",config.parameters[i].address,*r);
				break;
		}
	}	
}

int allocate_memory_space()
{
	mylog_debug("FCT allocate_memory_space()");
	
    mb_mapping = modbus_mapping_new_start_address(
		config.bits_address,
		config.bits_nb,
		config.input_bits_address,
		config.input_bits_nb,
		config.registers_address,
		config.registers_nb,
		config.input_registers_address,
		config.input_registers_nb);

    if (mb_mapping == NULL) 
    {
        mylog_error("Failed to allocate the mapping: %s\n", modbus_strerror(errno));
        return(-1);
    }
    return(0);
}

int reload_config()
{
	if(parseConfigFile(szConfigFile)) {
		iFin = 1;
		return(-1);
	}
	
	mylog_debug("FCT reload_config()");
	
	int rc = allocate_memory_space();
	if (rc) {
		iFin = 1;
		return(-2);
	}
	initialize_register();
	return(0);

}

void sig_handler(int signo)
{	
	switch(signo)
	{
		case SIGINT:
			mylog_warn("Signal SIGINT received");
			iFin = 1;
			break;

		case SIGUSR1:
			mylog_warn("Signal SIGUSR1 received");
			if (reload_config() != 0)
			{
				mylog_error("Error reloading config");
				iFin = 1;
			}
			break;
	}
}

int randInRange(int min, int max)
{
	return min + (int) (rand() % ((max - min) + 1)) ;
}



void update_register()
{
	uint8_t *b;
	uint16_t *r;
	long long new_time = current_timestamp();

	if ((new_time - time_ref) < config.update_freq) return;
	time_ref = new_time;
	
	mylog_debug("FCT update_register()");
	for (int i=0;i<config.nb_params;i++)
	{
		
		switch (config.parameters[i].type)
		{
			case TYPE_BITS:
				b = mb_mapping->tab_bits + (config.parameters[i].address - config.bits_address) ;
				break;

			case TYPE_INPUT_BITS:
				b = mb_mapping->tab_input_bits + (config.parameters[i].address - config.input_bits_address) ;
				break;

			case TYPE_REGISTERS:
				r = mb_mapping->tab_registers + (config.parameters[i].address - config.registers_address) ;
				break;

			case TYPE_INPUT_REGISTERS:
				r = mb_mapping->tab_input_registers + (config.parameters[i].address - config.input_registers_address) ;
				break;
		}

		switch (config.parameters[i].type)
		{
			case TYPE_BITS:
			case TYPE_INPUT_BITS:
				switch (config.parameters[i].update)
				{
					case UPDATE_ALTERNATE:
						*b ^= 1;
						break;
						
					case UPDATE_RANDOM:
						*b = randInRange(0,1);
						break;
				}
				break;
				
				
			case TYPE_REGISTERS:
			case TYPE_INPUT_REGISTERS:
				switch (config.parameters[i].update)
				{
					case UPDATE_INCREMENT:
						*r = *r + 1; // *r ++  focntionne pas !!! Pourquoi ?
						if (*r > config.parameters[i].max) *r = config.parameters[i].reset;
						break;
						
					case UPDATE_DECREMENT:
						*r = *r - 1;
						if (*r < config.parameters[i].min) *r = config.parameters[i].reset;
						break;
						
					case UPDATE_RANDOM:
						*r = randInRange(config.parameters[i].min,config.parameters[i].max);
						break;
				}
				break;		
		}
		
		switch (config.parameters[i].type)
		{
			case TYPE_BITS:
				mylog_debug("BIT at %d = %d",config.parameters[i].address,*b);
				break;
				
			case TYPE_INPUT_BITS:
				mylog_debug("INPUT BIT at %d = %d",config.parameters[i].address,*b);
				break;

			case TYPE_REGISTERS:
				mylog_debug("REGISTER at %d = %d",config.parameters[i].address,*r);
				break;
				
			case TYPE_INPUT_REGISTERS:
				mylog_debug("INPUT REGISTER at %d = %d",config.parameters[i].address,*r);
				break;
		}
	}	
}

void server_loop()
{
	fd_set	srvset,cltset;
	modbus_t *ctx;
	int server_socket;
	int client_socket;
	struct	timeval	tm_server,tm_client;
	int s;
	int rc;
	long long new_time = current_timestamp();

	mylog_debug("FCT server_loop()");
	
	/* To listen any addresses on port 502 */
	ctx = modbus_new_tcp("127.0.0.1", config.port);

	/* Handle until 10 MAX_CONNEXION connections */
	server_socket = modbus_tcp_listen(ctx, MAX_CONNEXION);

	while (!iFin)
	{
		FD_ZERO(&srvset);
		FD_SET(server_socket, &srvset);

		tm_server.tv_sec	= 0;
		tm_server.tv_usec	= 1000;		
		
		int ret = select(server_socket + 1,&srvset,NULL,NULL,&tm_server);
		if (ret == 1)
		{
			mylog_debug("Connection received");
			s = modbus_tcp_accept(ctx, &server_socket);
			if (s == -1) 
			{
				mylog_warn("Connection not accepted : %s",modbus_strerror(errno));
			} else
			{
				client_socket = modbus_get_socket(ctx);

				while (iFin == 0) 
				{
					uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
					
					// Il ne faut rester 'bloqué' sur le modbus_receive
					FD_ZERO(&cltset);
					FD_SET(client_socket, &cltset);

					tm_client.tv_sec	= 0;
					tm_client.tv_usec	= 1000;		

					int msg = select(client_socket + 1,&cltset,NULL,NULL,&tm_client);
					if (msg == 1)
					{
						rc = modbus_receive(ctx, query);
						if (rc > 0) 
						{
							/* rc is the query size */
							modbus_reply(ctx, query, rc, mb_mapping);
						} else if (rc == -1) 
						{
							close(s);
							break;
						}
					} else
						update_register();
				}
			}
		} else
			update_register();
	}

	close(server_socket);
	modbus_free(ctx);	
	modbus_mapping_free(mb_mapping);
}

static void help()
{
	printf("modbus-simulator ver %d.%d - (C) SQLI 2020\n",VERSION_MAJ,VERSION_MIN);
	printf("\tsyntax is : modbus-simulator --conf config.json\n");
	exit(-1);
}

int main( int argc, const char** argv )
{
	int rc;
	const char **p = argv;

	mylog_init("/dev/stdout", LOG_DEBUG);

	iFin = 0;
	strcpy(szConfigFile,"");
	while (*p != NULL)
	{
		if (strcmp(*p,"--conf") == 0)
		{
			*p ++;
			if (*p == NULL) help();
			strcpy(szConfigFile,*p);
		}
		*p ++;
	}
	if (*szConfigFile == '\0') help();

	rc = reload_config();
	mylog_info("Starting modbus-simulator daemon ver %d.%d (pid=%d)",VERSION_MAJ,VERSION_MIN,getpid());

	if (rc != 0)
	{
		switch (rc)
		{
			case ERROR_FILE :
				mylog_error("Unable to open configuration file %s",szConfigFile);
				break;
				
			case ERROR_PARSING :
				mylog_error("Invalid JSON in file %s",szConfigFile);
				break;
				
			case ERROR_PARAMETER :
				mylog_error("Bad parameter in file %s",szConfigFile);
				break;
				
			case ERROR_MISSING_VALUE :
				mylog_error("Missing value in file %s",szConfigFile);
				break;
				
			case ERROR_BAD_VALUE :
				mylog_error("+Bad value in file %s",szConfigFile);
				break;
				
			default:
				mylog_error("Loading configuration error: %d",rc);
				break;
		}
		exit(-1);
	}

	signal(SIGINT,sig_handler);
	signal(SIGUSR1,sig_handler);
	
	server_loop();

	mylog_info("Terminating modbus-simulator daemon");
	return(0);
}

