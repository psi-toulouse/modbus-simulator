# Modbus Simulator
This tool acts as a Modbus Server.
It is  also possible to declare variables and update them along time through a configuration file

## Features
modbus-simulatrr accept up to 16 modbus connection over TCP/IP  
Listening port is configurable  
Memory space and range is configurable for the 4 standards:
- Bits
- Input Bits
- Registers
- Input Registers

You can configure up to 1024 variables.  
Each variable is updated at a predefined frequence (the same for all variables)  

### Update modes
Registers and Input Registers can be :
- Randomized
- Incremented
- Decremented

Bits and Input Bits can be:
- Randomized
- Alternated

## Compilation from sources

### Clone the projet
```
$ cd ~ 
$ git clone <url>
```

### Dependencies
Before compiling source code, you shall install some dependencies.  

#### Basics
- `sudo apt-get install -y build-essential`
- `sudo apt-get install -y cmake`
- `sudo apt-get install -y pkg-config`
- `sudo apt-get install -y libmosquitto-dev`

#### Specifics
If you want to build a standalone binary for the simulator (no dynamic library requiered), you shall use only statics libraries during the compilation stage.  

- libjson-c-dev : _refer to https://github.com/json-c/json-c/blob/master/README.md for building a static library_
Take care to build json-c with this command : `cmake -DBUILD_SHARED_LIBS=OFF ../jscon-c`to avoid shared library issue. Then go to jsonc-c-build directory and launch '`sudo make install`'

- libmodbus-dev _refer to https://libmodbus.org/download/ for building a static library_
Take care to run `./configure --enable-static --enable-shared=no` to force static library.

- uuid-dev (`sudo apt-get install -y uuid-dev`)

**WARNING : Concerning libmodbus-dev you SHALL use at least 3.1.6 version.**


### Build the project
The easiest step ...
```
$ cd src
$ make
$ cp modbus-simulator ..
```
## Configuration

### Full File sample
```
{
	"log_path": "./modbus-simulator.log",
	"log_level": 5,

	"port": 5020,
	"update_freq": 1,
	
	"bits_address": 0,
	"bits_nb": 500,
	
	"input_bits_address": 0,
	"input_bits_nb": 100,
	
	"registers_address": 0,
	"registers_nb": 500,
	
	"input_registers_address": 0,
	"input_registers_nb": 100,
	
	"bits" :
   	[
		{ "address":100, "set":1, "update" : "random" },
		{ "address":101, "set":1, "update" : "alternate" }
	],
	
	"registers" :
   	[
		{ "address":150, "set":8,
			"update" : 
			{ "random" :
				{ "min" : 0, "max" : 19 }
			}
		},
		
		{ "address":149, "set":0,
			"programmable" : 
			{ "timer" :
				{ "second" : 300, "value" : 45 }
			}
		},

		{ "address":151, "set":32760, 
			"update": 
			{ "increment": 
				{ "max": 32767, "reset": 1 } 
			}
		}
	],

	"input_bits" :
   	[
	],

	"input_registers" :
   	[
	]	
}
```

### JSON Key description

**log_path**
- Full path file for logs

**log_level**
- Log level (0=CRITIAL, 1=ERROR, 2=WARNING, 3=INFO, 4=DEBUG, 5=NO LOG)

**port**
- Listen port (default = 502)

**update_freq**
- Variable update frequence in seconds

**bits_address**
- Starting address for bits memory range

**bits_nb**
- Memory size for bits

**input_bits_address**
- Starting address for input bits memory range

**input_bits_nb**
- Memory size for input bits
	
**registers_address**
- Starting address for registers memory range

**registers_nb**
- Memory size for registers

**input_registers_address**
- Starting address for input registers memory range

**input_registers_nb**
- Memory size for input registers

**registers**
- Collection of declared registers

**input_registers**
- Collection of declared input registers

**bits**
- Collection of declared bits

**input_bits**
- Collection of declared input bits

#### Variables attributes

##### address
- Variable address [starting_address , (starting_address + variables_number - 1)]  = Mandatory

##### set
- Initial value (Mandatory)

##### update
- Declare an updatable variable (Optional)

###### random
- Variable is set in the range [**min**, **max**] for registers / input_registers or [**0**, **1**] for bits / input_bits

###### increment
- Variable is incremented and reset to **reset** value where greater than **max**

###### decrement
- Variable is decremented and reset to **reset** value where lower than **min**

###### alternate
- Variable aleternate between 0 and 1 at each update. Only applicable to bits / input_bits variables

## Execution sample
```
$ ./modbus-simulator --conf json/test_simulator.json
```

